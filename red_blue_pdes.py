#!/usr/bin/env python

import argparse
import sys
import json

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.special import eval_legendre


def integrate_optimalcontrol(Nt=8000,
        dx=0.025,
        xmin=-2,
        xmax=2,
        lambduh=0.,
        sigma=0.5,
        bthresh=0.,
        reruns=100,
        final='quad',
        vmi=None):
    """
    Integrate single HJB equation corresponding with other player 
    having credible commitment to particular strategy.
    """
    xmax = int(xmax)
    xmin = int(xmin)
    dt = (1/5) * dx ** 2  # this would be unconditionally stable if pde were linear
    Nx = int( (xmax - xmin) / dx )
    x = np.linspace(xmin, xmax, Nx)
    time = np.linspace(0, Nt * dt, Nt)

    v = np.ones( (Nt, Nx + 2) )

    if final == 'asym':
        v[0, 1:-1] = np.where(x < bthresh, np.absolute(x - bthresh)**2, 0)
    elif final == 'piecewise':
        v[0, 1:-1] = np.where(np.absolute(x) < bthresh, -2, 2)
    elif final == 'zero':
        v[0, 1:-1] = 0.
    elif final == 'tanh':
        v[0, 1:-1] = np.tanh(x)
    else:
        v[0, 1:-1] = 0.5 * (x - bthresh)**2

    # set up other strategy
    if vmi is None:
        vmi = np.zeros(Nt)

    # set up boundaries
    v[0, 0] = v[0, 1]
    v[0, -1] = v[0, -2]

    # integrate using backward FDM
    for i in range(1, len(time)):
        # do the interior
        for j in range(1, x.shape[0] + 1):
            # define derivatives
            dvdx = (v[i - 1, j + 1] - v[i - 1, j - 1]) / (2 * dx)
            dv2dx2 = (v[i - 1, j + 1] - 2 * v[i - 1, j] + v[i - 1, j - 1]) / dx**2

            # define sides
            rhs = -0.25 * dvdx **2 + vmi[i] * dvdx + lambduh * vmi[i]**2\
                    + sigma **2 / 2 * dv2dx2

            # step forward
            v[i, j] = v[i - 1, j] + dt * rhs

        # now do the boundaries
        v[i, 0] = v[i, 1]
        v[i, -1] = v[i, -2]

    # now calculate sample trajectory
    # since we integrated backward, we have to roll forward
    logit_path = np.zeros( (reruns, Nt) )
    uncontrolled_path = np.zeros( (reruns, Nt) )
    u = np.zeros( (reruns, Nt) )
    logit_path[:, 0] = np.zeros(reruns)  # or some other ic
    uncontrolled_path[:, 0] = logit_path[:, 0]

    for i in range(len(time) - 1):
        # first find optimal controls
        # ur = -0.5 * dvrdx
        # ub = -0.5 * dvbdx
        pos = np.digitize( logit_path[:, i], x)

        # have to iterate backward
        dvdx = np.diff(v[Nt - i - 1, :])
        u_ = np.array( [-0.5 * dvdx[p] / dx for p in pos] )

        # get the random state since we want to show the uncontrolled version
        vol_v = sigma * np.sqrt(dt) * np.random.randn(reruns)

        # now iterate the state
        logit_path[:, i + 1] = logit_path[:, i] + dt * ( u_ + vmi[i] ) \
                + vol_v
        uncontrolled_path[:, i + 1] = uncontrolled_path[:, i] + vol_v
        u[:, i] = u_

    return v, x, time, u, logit_path, uncontrolled_path 


def integrate(Nt=8000,
        dx=0.025,
        xmin=-2,
        xmax=2,
        lambduh_r=0.,
        lambduh_b=0.,
        sigma=0.5,
        bthresh=0.,
        reruns=100,
        finalr='linear',
        finalb='quad',
        x0=0.,
        drift=np.zeros(2),
        redcost='none',
        bluecost='none',
        ):
    """
    Integrate system of HJB equations.
    """
    xmax = int(xmax)
    xmin = int(xmin)
    dt = (1/5) * dx ** 2  # this would be unconditionally stable if pde were linear
    Nx = int( (xmax - xmin) / dx )
    x = np.linspace(xmin, xmax, Nx)
    time = np.linspace(0, Nt * dt, Nt)
    
    vr = np.ones( (Nt, Nx + 2) )
    vb = np.ones( (Nt, Nx + 2) )
    
    # check passed drift values
    if drift is None:
        drift = np.zeros(2)
    drift = np.array(drift, dtype=float)
    assert drift.shape == (2,)

    # set up running cost if it is passed
    if redcost == 'piecewise':
        redcost = lambda x: -1. if x < 0 else 1.
    elif redcost == 'none':
        redcost = lambda x: 0.
    else:
        raise ValueError('redcost must be one of "none" or "piecewise"')

    if bluecost == 'piecewise':
        bluecost = lambda x: 1. if x < 0 else -1.
    elif bluecost == 'none':
        bluecost = lambda x: 0.
    else:
        raise ValueError('bluecost must be one of "none" or "piecewise"')

    # set up final conditions
    # red
    if finalr == 'tanh':
        vr[0, 1:-1] = 2 * np.tanh(x)
    elif finalr == 'piecewise':
        vr[0, 1:-1] = np.where(x < 0, -2, 2)
    elif finalr == 'linear':
        vr[0, 1:-1] = x
    elif not callable(finalr):
        # then these are coefficients of the legendre polynomial
        # if you're using this, you know what you are doing
        # so you should have set xmin = -xmax (symmetry is important here)
        X = np.array([eval_legendre(n, x/xmax) for n in range(finalr.shape[0])]).T  # for now constant order
        # finalr are assumed to be the coefficients of this expansion
        vr[0, 1:-1] = np.dot(X, finalr)
    else:
        vr[0, 1:-1] = finalr(x)

    # blue
    if finalb == 'asym':
        vb[0, 1:-1] = np.where(x < bthresh, np.absolute(x - bthresh)**2, 0)
    elif finalb == 'piecewise':
        vb[0, 1:-1] = np.where(np.absolute(x) < bthresh, -2, 2)
    elif finalb == 'quad':
        vb[0, 1:-1] = 0.5 * (x - bthresh)**2
    elif not callable(finalb):
        # then these are coefficients of the legendre polynomial
        # if you're using this, you know what you are doing
        # so you should have set xmin = -xmax (symmetry is important here)
        X = np.array([eval_legendre(n, x/xmax) for n in range(finalb.shape[0])]).T  # for now constant order
        # finalb are assumed to be the coefficients of this expansion
        vb[0, 1:-1] = np.dot(X, finalb)
    else:
        vb[0, 1:-1] = finalb(x)

    # set up boundaries
    vr[0, 0] = vr[0, 1]
    vr[0, -1] = vr[0, -2]
    vb[0, 0] = vb[0, 1]
    vb[0, -1] = vb[0, -2]

    # integrate using backward FDM
    for i in range(1, len(time)):
        # do the interior
        for j in range(1, x.shape[0] + 1):
            # define derivatives
            dvrdx = (vr[i - 1, j + 1] - vr[i - 1, j - 1]) / (2 * dx)
            dvbdx = (vb[i - 1, j + 1] - vb[i - 1, j - 1]) / (2 * dx)
            dvr2dx2 = (vr[i - 1, j + 1] - 2 * vr[i - 1, j] + vr[i - 1, j - 1]) / dx**2
            dvb2dx2 = (vb[i - 1, j + 1] - 2 * vb[i - 1, j] + vb[i - 1, j - 1]) / dx**2

            # define sides
            rhs_r = -0.25 * dvrdx **2 - 0.5 * dvrdx * dvbdx - 0.25 * lambduh_r  * dvbdx **2\
                    + sigma **2 / 2 * dvr2dx2\
                    + (drift[0] + drift[1] * x[j - 1]) * dvrdx\
                    + redcost(x[j - 1])
            rhs_b = -0.25 * dvbdx **2 - 0.5 * dvbdx * dvrdx - 0.25 * lambduh_b  * dvrdx **2\
                    + sigma **2 / 2 * dvb2dx2\
                    + (drift[0] + drift[1] * x[j - 1]) * dvbdx\
                    + bluecost(x[j - 1])\

            # step forward
            vr[i, j] = vr[i - 1, j] + dt * rhs_r
            vb[i, j] = vb[i - 1, j] + dt * rhs_b

        # now do the boundaries
        vr[i, 0] = vr[i, 1]
        vr[i, -1] = vr[i, -2]
        vb[i, 0] = vb[i, 1]
        vb[i, -1] = vb[i, -2]


    # now calculate sample trajectory
    # since we integrated backward, we have to roll forward
    logit_path = np.zeros( (reruns, Nt) )
    uncontrolled_path = np.zeros( (reruns, Nt) )
    ur = np.zeros( (reruns, Nt) )
    ub = np.zeros( (reruns, Nt) )
    logit_path[:, 0] = x0 * np.ones(reruns)  # or some other ic
    uncontrolled_path[:, 0] = logit_path[:, 0]

    for i in range(len(time) - 1):
        # first find optimal controls
        # ur = -0.5 * dvrdx
        # ub = -0.5 * dvbdx
        pos = np.digitize( logit_path[:, i], x)

        # have to iterate backward
        dvrdx = np.diff(vr[Nt - i - 1, :])
        dvbdx = np.diff(vb[Nt - i - 1, :])
        ur_ = np.array( [-0.5 * dvrdx[p] / dx for p in pos] )
        ub_ = np.array( [-0.5 * dvbdx[p] / dx for p in pos] )

        # get the random state since we want to show the uncontrolled version
        vol_rv = sigma * np.sqrt(dt) * np.random.randn(reruns)

        # now iterate the state
        logit_path[:, i + 1] = logit_path[:, i] + dt * ( ur_ + ub_ ) \
                + vol_rv
        uncontrolled_path[:, i + 1] = uncontrolled_path[:, i] + vol_rv
        ur[:, i] = ur_
        ub[:, i] = ub_

    return vb, vr, x, time, ur, ub, logit_path, uncontrolled_path 


def parse_args(args):

    parser = argparse.ArgumentParser(description='HJB integrator for red/blue election game.')
    
    parser.add_argument('--id',
            type=int,
            help='id number of run'
            )
    parser.add_argument('--reruns',
            default=100,
            type=int,
            help='number of simulations of optimal control paths'
            )
    parser.add_argument('--nt',
            default=8000,
            type=int,
            help='number of timesteps for which to integrate'
            )
    parser.add_argument('--dx',
            default=0.025,
            type=float,
            help='spatial discretization step'
            )
    parser.add_argument('--xbound',
            default=2,
            type=float,
            help='spatial bound, min will be -xbound, max will be xbound'
            )
    parser.add_argument('--lr',
            default=0.,
            type=float,
            help='linkage parameter for red'
            )
    parser.add_argument('--lb',
            default=0.,
            type=float,
            help='linkage parameter for blue'
            )
    parser.add_argument('--sigma',
            default=0.6,
            type=float,
            help='volatility coefficient, ideally should be greater than 0.5 for stability'
            )
    parser.add_argument('--bthresh',
            default=0.,
            type=float,
            help='threshold for blue "caring" about result'
            )
    parser.add_argument('--finalcondr',
            default='linear',
            type=str,
            help="red's final condition. Default options are 'linear' f(x) = x, 'piecewise' f(x) = -2 if x < 0 else 2, 'tanh' f(x) = 2 * tanh(x)"
            )
    parser.add_argument('--finalcondb',
            default='quad',
            type=str,
            help="blue's final condition. Options are 'quad' f(x) = 0.5 x**2, 'piecewise' f(x) = -2 if |x| < bthresh else 2,"\
                    "'asym' f(x) = 0 if x > bthresh else 0.5 * x**2"
                )
    parser.add_argument(
        '--drift',
        nargs='*',
        action='store',
        help='drift coefficients \mu_0 and \mu_1. Pass either neither or both values.',
            )
    parser.add_argument(
        '--redcost',
        type=str,
        default='none',
        help='string corresponding to additional Red state-dependent running cost function.'
        ' Options are "none" for no running cost and "piecewise" for -1 if x < 0 else 1'
            )
    parser.add_argument(
        '--bluecost',
        type=str,
        default='none',
        help='string corresponding to additional Blue state-dependent running cost function.'
        ' Options are "none" for no running cost and "piecewise" for 1 if x < 0 else -1'
            )
    parser.add_argument('--datapath',
            default='../data/red-blue-game',
            type=str,
            help='path to which to save data'
            )
    parser.add_argument('--figpath',
            default='../paper/figures/red-blue-election-game',
            type=str,
            help='path to which to save figure'
            )
    parsed_args = parser.parse_args( args )

    if parsed_args.drift is not None and len(parsed_args.drift) not in [0, 2]:
        parser.error(f'Either pass zero or two values for drift, not {len(args.drift)}')

    return parsed_args


if __name__ == "__main__":

    args = parse_args( sys.argv[1:] )

    kwargs = {
            'Nt' : args.nt,
            'dx' : args.dx,
            'xmin' : -1.0 * args.xbound,
            'xmax' : args.xbound,
            'lambduh_r' : args.lr,
            'lambduh_b' : args.lb,
            'sigma' : args.sigma,
            'bthresh' : args.bthresh,
            'reruns' : args.reruns,
            'finalr' : args.finalcondr,
            'finalb' : args.finalcondb,
            'drift' : args.drift,
            'redcost' : args.redcost,
            'bluecost' : args.bluecost
            }

    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path = integrate(Nt=args.nt,
            dx=args.dx,
            xmin=-1.0 * args.xbound,
            xmax=args.xbound,
            lambduh_r=args.lr,
            lambduh_b=args.lb,
            sigma=args.sigma,
            bthresh=args.bthresh,
            reruns=args.reruns,
            finalr=args.finalcondr,
            finalb=args.finalcondb,
            drift=args.drift,
            redcost=args.redcost,
            bluecost=args.bluecost,
            )

    fig, axes = plt.subplots(1, 2, figsize=(20, 7))
    fig.subplots_adjust(left=0.02, bottom=0.06, right=0.95, top=0.94, wspace=0.05)
    ax, ax2 = axes

    im = ax.imshow(np.flip(vr.T, axis=1), aspect='auto', origin='lower',
            vmin=np.min(vr), vmax=np.max(vr),
            cmap=plt.cm.magma,
            extent=[0, 1, -1.0 * args.xbound, args.xbound])
    ax.set_xlabel('$t$ (years)', fontsize=25)
    ax.set_ylabel('$x$ (latent variable)', fontsize=25)
    ax.tick_params(labelsize=20)
    cbar1 = fig.colorbar(im, ax=ax)
    cbar1.ax.set_ylabel('$V_R(x,t)$', fontsize=25)
    cbar1.ax.tick_params(labelsize=20)

    im = ax2.imshow(np.flip(vb.T, axis=1), aspect='auto', origin='lower',
            vmin=np.min(vb), vmax=np.max(vb),
            cmap=plt.cm.magma,
            extent=[0, 1, -1.0 * args.xbound, args.xbound])
    ax2.set_xlabel('$t$ (years)', fontsize=25)
    ax2.set_yticks([])
    ax2.set_yticklabels([])
    ax2.tick_params(labelsize=20)
    cbar2 = fig.colorbar(im, ax=ax2)
    cbar2.ax.set_ylabel('$V_B(x,t)$', fontsize=25)
    cbar2.ax.tick_params(labelsize=20)
    
    # save output
    fname = f'{args.id}'
    np.savez_compressed(
            f'{args.datapath}/{fname}.npz',
            logit_path=logit_path,
            uncontrolled_path=uncontrolled_path,
            ur=ur,
            ub=ub,
            vr=vr,
            vb=vb,
            time=time
            )
    with open(f'{args.datapath}/{fname}.json', 'w') as f:
        json.dump( kwargs, f, sort_keys=True, indent=4 )
    
    plt.savefig(f'{args.figpath}/{fname}.png',
            bbox_inches='tight',
            transparent=True)
    plt.savefig(f'{args.figpath}/{fname}.pdf',
            bbox_inches='tight')
