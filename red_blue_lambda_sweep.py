#!/usr/bin/env python

import itertools

import numpy as np
np.random.seed( 123 )

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import pathos
import joblib

from red_blue_pdes import integrate as hjb_int


def lambda_param_sweep( 
        lrs,
        lbs,
        finalr,
        finalb,
        Nt=8000,
        dx=0.025,
        xmin=-3,
        xmax=3,
        sigma=1.,
        reruns=1000,
        x0=0.,
        bthresh=0.25,
        kind='std'
        ):

    sweep_r = np.zeros( (lrs.shape[0], lbs.shape[0]) )
    sweep_b = np.zeros( (lrs.shape[0], lbs.shape[0]) )
    fn = getattr(np, kind)  # operation to calculate on each time series

    def job( x ):
        rr, bb = x
        i, lr = rr
        j, lb = bb
        vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=dx,
                    xmin=xmin,
                    xmax=xmax,
                    lambduh_r=lr,
                    lambduh_b=lb,
                    sigma=sigma,
                    reruns=reruns,
                    finalr=finalr,
                    finalb=finalb,
                    bthresh=bthresh,
                    x0=x0)
        
        mean_quad_ur = np.mean( fn( -ur, axis=0 ) )  # 
        mean_quad_ub = np.mean( fn( ub, axis=0 ) )

        return i, j, mean_quad_ur, mean_quad_ub


    params = itertools.product( enumerate(lrs), enumerate(lbs) )
    pool = pathos.multiprocessing.ProcessingPool( None )
    results = pool.map( job, params )
    joblib.dump(
        results,
        f'../data/red-blue-game/lambda-sweep/{kind}-{finalr}-{finalb}.gz'
            )

    for res in results:
        i, j, r, b = res
        sweep_r[i, j] = r
        sweep_b[i, j] = b

    return sweep_r, sweep_b


def main(kind='std'):
    lrs = np.linspace(0, 3, 10)
    lbs = np.linspace(0, 3, 10)
    finalrs = ['tanh', 'linear', 'piecewise']
    finalbs = ['asym', 'quad', 'piecewise']
    bthresh = 0.1

    xvals = np.linspace(-3, 3, 100)

    for i, finalr in enumerate( finalrs ):
        for j, finalb in enumerate( finalbs ):
            fig = plt.figure( figsize=(8, 12) )
            figshape = (2, 1)
            axes = [plt.subplot2grid( figshape, (i, 0) ) for i in range(2)]
            
            # actually do the integration
            sweep_r, sweep_b = lambda_param_sweep( lrs, lbs, finalr, finalb, bthresh=bthresh, kind=kind )

            ax, ax2 = axes
            LBS, LRS = np.meshgrid(lbs, lrs)

            im = ax.contourf(LBS, LRS, sweep_r,
                    extent=[0, 3, 0, 3],
                    cmap='Reds',
                    origin='lower')
            ax.contour(im, colors='darkgray')
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='5%', pad=0.05)
            cbar = fig.colorbar(im, cax=cax, orientation='vertical')
            cax.set_ylabel(f'$\mathrm{{{kind}}}(u_R(t))$', fontsize=20)
            cax.tick_params(labelsize=20)

            axins = inset_axes(ax, width="25%", height="25%", loc=1)
            if finalr == 'tanh':
                axins.plot(xvals, 2 * np.tanh( xvals ), 'black', linewidth=2 )
                axins.plot(xvals, 2 * np.tanh( xvals ), 'white' )
            elif finalr == 'linear':
                axins.plot(xvals, xvals, 'black', linewidth=2)
                axins.plot(xvals, xvals, 'white')
            else:
                axins.plot(xvals, np.where(xvals < 0, -2, 2), 'black', linewidth=2 )
                axins.plot(xvals, np.where(xvals < 0, -2, 2), 'white' )
            axins.axis('off')

            im2 = ax2.contourf(LBS, LRS, sweep_b,
                    extent=[0, 3, 0, 3],
                    cmap='Blues',
                    origin='lower')
            ax2.contour(im2, colors='darkgray')
            divider2 = make_axes_locatable(ax2)
            cax2 = divider2.append_axes('right', size='5%', pad=0.05)
            cbar2 = fig.colorbar(im2, cax=cax2, orientation='vertical')
            cax2.set_ylabel(f'$\mathrm{{{kind}}}(u_B(t))$', fontsize=20)
            cax2.tick_params(labelsize=20)

            axins2 = inset_axes(ax2, width="25%", height="25%", loc=1)
            if finalb == 'asym':
                axins2.plot(xvals,
                        np.where(xvals < bthresh, np.absolute(xvals - bthresh)**2, 0), 'black', linewidth=2 )
                axins2.plot(xvals,
                        np.where(xvals < bthresh, np.absolute(xvals - bthresh)**2, 0), 'white' )
            elif finalb == 'piecewise':
                axins2.plot(xvals, np.where(np.absolute(xvals) < bthresh, -2, 2), 'black', linewidth=2)
                axins2.plot(xvals, np.where(np.absolute(xvals) < bthresh, -2, 2), 'white')
            else:
                axins2.plot(xvals, 0.5 * xvals**2., 'black', linewidth=2 )
                axins2.plot(xvals, 0.5 * xvals**2., 'white' )
            axins2.axis('off')

            for a in axes:
                a.set_xlabel('$\lambda_B$', fontsize=20)
                a.set_ylabel('$\lambda_R$', fontsize=20)
                a.tick_params(labelsize=20)

            fig.tight_layout()
            fig.savefig(f'./tmp/lambda-param-sweep-{kind}-{finalr}-{finalb}.png',
                    bbox_inches='tight',
                    transparent=True)
            fig.savefig(f'./tmp/lambda-param-sweep-{kind}-{finalr}-{finalb}.pdf',
                    bbox_inches='tight')
            plt.close()



if __name__ == "__main__":

    kind = 'mean'
    main( kind=kind )
