# Noncooperative dynamics in election interference

This repository contains code to recreate simulations and figures in 
[Noncooperative dynamics in election interference](https://arxiv.org/abs/1908.02793).
Readers of this code will probably be sad that most paths are absolute and hence you have to go through the files 
and change where you want to save figures in order to see them. Such is life. 

However, the central piece of software here, `red_blue_pdes.py`, happily does not suffer from this flaw. 
Here is output from its help string:
```
(base) ip0af5cbaf:red-blue-code dave$ ./red_blue_pdes.py -h
usage: red_blue_pdes.py [-h] [--id ID] [--reruns RERUNS] [--nt NT] [--dx DX]
                        [--xbound XBOUND] [--lr LR] [--lb LB] [--sigma SIGMA]
                        [--bthresh BTHRESH] [--finalcondr FINALCONDR]
                        [--finalcondb FINALCONDB] [--datapath DATAPATH]
                        [--figpath FIGPATH]

HJB integrator for red/blue election game.

optional arguments:
  -h, --help            show this help message and exit
  --id ID               id number of run
  --reruns RERUNS       number of simulations of optimal control paths
  --nt NT               number of timesteps for which to integrate
  --dx DX               spatial discretization step
  --xbound XBOUND       spatial bound, min will be -xbound, max will be xbound
  --lr LR               linkage parameter for red
  --lb LB               linkage parameter for blue
  --sigma SIGMA         volatility coefficient, ideally should be greater than
                        0.5 for stability
  --bthresh BTHRESH     threshold for blue "caring" about result
  --finalcondr FINALCONDR
                        red's final condition. Default options are 'linear'
                        f(x) = x, 'piecewise' f(x) = -2 if x < 0 else 2,
                        'tanh' f(x) = 2 * tanh(x)
  --finalcondb FINALCONDB
                        blue's final condition. Options are 'quad' f(x) = 0.5
                        x**2, 'piecewise' f(x) = -2 if |x| < bthresh else
                        2,'asym' f(x) = 0 if x > bthresh else 0.5 * x**2
  --datapath DATAPATH   path to which to save data
  --figpath FIGPATH     path to which to save figure
```

Okay, fine. The system of equations that are integrated are a set of nonlinear parabolic equations. 
They depend quadratically on their own and the other equation's first spatial derivative and so can be 
a little unstable for low sigma.

### Requirements
Reasonably-new versions of the below should be fine.
+ `numpy`
+ `scipy`
+ `matplotlib`
+ `joblib`
+ `pathos` (better `multiprocessing` clone)
+ `scikit-optimize` (for working with real data, not necessary for simulations)
+ `pandas` (for working with real data, not necessary for simulations)
+ `pymc3` (for working with real data, not necessary for simulations)
