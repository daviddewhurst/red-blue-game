#!/usr/bin/env python

import numpy as np
np.random.seed( 123 )
import matplotlib.pyplot as plt

from red_blue_pdes import integrate as hjb_int


def main():

    # global constant parameters
    # we are interested in the high-coupling region
    xmin = -3.
    xmax = -xmin
    lambduh_r = 3.
    lambduh_b = 3.
    sigma = 1.
    Nt = 8000
    x0 = 0.
    bthresh = 1.
    pct_l = 10
    pct_h = 90
    pct = [pct_l, pct_h]

    fig, axes = plt.subplots(1, 3, figsize=(15, 5))
    axes = tuple( axes.flatten() )
    ax2, ax3, ax4 = axes


    # first simulation, part 2
    finalr = 'piecewise'
    finalb = 'asym'
    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=500,
                    finalr=finalr,
                    finalb=finalb,
                    x0=x0)

    
    mean_ur = np.mean( -ur, axis=0 )
    mean_ub = np.mean( ub, axis=0 )

    pct_r = np.percentile(-ur, pct, axis=0)
    pct_b = np.percentile(ub, pct, axis=0)

    ax3.plot(time[:-1], mean_ur[:-1], 'red', linestyle='--',
            label='$E[u_{\mathrm{{R}}}]$, discontinuous\nRed final cond.')

    ax3.plot(time[:-1], mean_ub[:-1], 'blue', linestyle='--',
             label='$E[u_{\mathrm{{B}}}]$, discontinuous\nRed final cond.')

    ax3.fill_between(
            time[:-1],
            pct_r[0][:-1],
            pct_r[1][:-1],
        facecolor="r",
        alpha=0.25,
            )

    ax3.fill_between(
            time[:-1],
            pct_b[0][:-1],
            pct_b[1][:-1],
        facecolor="b",
        alpha=0.25,
            )
    ax3.set_yscale('symlog')

    # second simulation, part 1
    finalr = 'tanh'
    finalb = 'asym'
    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=500,
                    finalr=finalr,
                    finalb=finalb,
                    x0=x0)
    
    mean_ur = np.mean( -ur, axis=0 )
    mean_ub = np.mean( ub, axis=0 )

    pct_r = np.percentile(-ur, pct, axis=0)
    pct_b = np.percentile(ub, pct, axis=0)

    ax2.plot(time[:-1], mean_ur[:-1], 'red',
            label='$E[u_{\mathrm{{R}}}]$, continuous\nBlue final cond.')
    ax2.plot(time[:-1], mean_ub[:-1], 'blue',
            label='$E[u_{\mathrm{{B}}}]$, continuous\nBlue final cond.')

    ax2.fill_between(
            time[:-1],
            pct_r[0][:-1],
            pct_r[1][:-1],
        color='red',
        alpha=0.25,
            )

    ax2.fill_between(
            time[:-1],
            pct_b[0][:-1],
            pct_b[1][:-1],
        color='blue',
        alpha=0.25,
            )
    ax2.set_yscale('symlog')

    # second simulation, part 2
    finalr = 'tanh'
    finalb = 'piecewise'
    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=500,
                    finalr=finalr,
                    finalb=finalb,
                    bthresh=1.,
                    x0=x0)
    
    mean_ur = np.mean( -ur, axis=0 )
    mean_ub = np.mean( ub, axis=0 )

    pct_r = np.percentile(-ur, pct, axis=0)
    pct_b = np.percentile(ub, pct, axis=0)

    ax4.plot(time[:-1], mean_ur[:-1], 'red', linestyle='--',
            label='$E[u_{\mathrm{{R}}}]$, discontinuous\nBlue final cond.')

    ax4.plot(time[:-1], mean_ub[:-1], 'blue', linestyle='--',
             label='$E[u_{\mathrm{{B}}}]$, discontinuous\nBlue final cond.')

    ax4.fill_between(
            time[:-1],
            pct_r[0][:-1],
            pct_r[1][:-1],
        facecolor="r",
        alpha=0.25,
            )

    ax4.fill_between(
            time[:-1],
            pct_b[0][:-1],
            pct_b[1][:-1],
        facecolor="b",
        alpha=0.25,
            )
    ax4.set_yscale('symlog')

    for a in axes:
        a.set_xlabel('$t$ (time, years)', fontsize=17)
        a.set_ylabel('$u(t)$ (control policy)', fontsize=17)
        a.tick_params(labelsize=17)
        a.legend(fontsize=17, loc=2, framealpha=0.)
        a.set_ylim(-85, 85)
        a.set_xlim(0,)

    
    plt.tight_layout()
    plt.savefig('./tmp/lambda-ts.png',
            bbox_inches='tight',
            transparent=True)
    plt.savefig('./tmp/lambda-ts.pdf',
            bbox_inches='tight')



if __name__ == "__main__":
    main()
