#!/usr/bin/env python

import sys

import joblib
import numpy as np
import matplotlib
matplotlib.rcParams['backend'] = "Qt4Agg"
import matplotlib.pyplot as plt
import pandas as pd
from scipy.special import eval_legendre

from red_blue_pdes import integrate as hjb_int


def logistic(x):
    return 1 / (1 + np.exp(-x))


def logit(x):
    return np.log( x / (1.0 - x) )


def main():

    run_num = int( sys.argv[1] )

    np.random.seed( 123 )  # for reproducibility, change this to whatever for marginally different distributions
    datapath = '../data/red-blue-game/' 
    figpath = '../paper/figures/red-blue-election-game/'

    # load the empirical data
    # enforce order red - blue - election for concatenation to higher-dim space
    latent_red_control = np.load(datapath + 'red_control_mean.npy')
    latent_blue_control = np.load(datapath + 'blue_control_mean.npy')
    real_election = np.load(datapath + 'logit_election_mean.npy')[:-1]  # one longer... and on (0, 1)

    # result of the optimization
    res = joblib.load( f'../data/red-blue-game/red-blue-abc/pde-MAP-gp-means-plus-std-newtime-{run_num}.gz' )

    lambduh_r = res['x'][0]
    lambduh_b = res['x'][1]
    sigma = res['x'][2]
    lcoef = int( len(res['x'][3:]) / 2 )
    red_coef = np.array( res['x'][3:3 + lcoef] )
    blue_coef = np.array( res['x'][3 + lcoef:] )
    x0 = logit(real_election[0])
    xmax = 2
    
    # recalculate Nt
    # it was roughly 8000 timesteps equals 364 days, but now we will reset this as we are only looking at length 102 vecs
    Nt = int( 8000 * len(latent_red_control) / 364 )
    Nt_downsamp = len(latent_red_control)


    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=500,
                    finalr=red_coef,
                    finalb=blue_coef,
                    x0=x0)

    # set up values for interpolation
    orig_x = np.linspace(0, 1, Nt)  # dimensionality of the model's ts
    downsamp_x = np.linspace(0, 1, Nt_downsamp)
    
    # now we have to downsample this to length 364, since that's what our empirical data is
    # we can shift the vectors around but not scale them
    ur = -1.0 * np.array( [np.interp(downsamp_x, orig_x, r) for r in ur] ) 
    ub = np.array( [np.interp(downsamp_x, orig_x, b) for b in ub] )
    election = np.array( [np.interp(downsamp_x, orig_x, e) for e in logit_path] )  # on (-\infty, \infty)

    mean_ur = np.mean( ur, axis=0 )
    mean_ub = np.mean( ub, axis=0 )
    mean_election = np.mean( election, axis=0 )

    # make the joint plot

    fig, ax = plt.subplots(figsize=(6, 12))
    
    #for r in ur:
    #    ax.plot(r, downsamp_x[::-1], 'r-', linewidth=0.1)

    #for b in ub:
    #    ax.plot(b, downsamp_x[::-1], 'b-', linewidth=0.1)

    pct_l = 10
    pct_h = 90
    pct = [pct_l, pct_h]

    pct_r = np.percentile(ur, pct, axis=0)
    pct_b = np.percentile(ub, pct, axis=0)

    ax.fill_betweenx(
        downsamp_x[::-1] * len(pct_r[0]),
        pct_r[0],
        pct_r[1],
        color='red',
        alpha=0.25,
        label=f'Middle {pct_h - pct_l}\n posterior\n $\hat{{u}}_{{\mathrm{{R}}}}$ percentiles'
            )

    ax.fill_betweenx(
        downsamp_x[::-1] * len(pct_b[0]),
        pct_b[0],
        pct_b[1],
        color='blue',
        alpha=0.25,
        label=f'Middle {pct_h - pct_l}\n posterior\n $\hat{{u}}_{{\mathrm{{B}}}}$ percentiles'
            )

    ax.plot(latent_red_control, downsamp_x[::-1] * len(pct_r[0]), 'red', linewidth=3,
            label='$E[u_{\mathrm{R}}]$',
            alpha=0.6)
    ax.plot(latent_blue_control, downsamp_x[::-1] * len(pct_b[0]), 'blue', linewidth=3,
            label='$E[u_{\mathrm{B}}]$',
            alpha=0.6)
    ax.legend(loc=2, fontsize=14,
            framealpha=0.)

    ax.set_xlim(-4, 4)
    ax.set_ylim(0, len(pct_b[0]))
    ax.set_xlabel('$u_i(t)$', fontsize=15)
    ax.set_ylabel('$T - t$ (time-to-go, days)', fontsize=15)
    ax.tick_params(labelsize=15)
    
    plt.tight_layout()
    plt.savefig('./tmp/abc-forest.png',
            transparent=True,
            bbox_inches='tight')
    plt.savefig('./tmp/abc-forest.pdf',
            bbox_inches='tight')
  

if __name__ == "__main__":
    main()
