#!/usr/bin/env python

import pickle

import numpy as np
import pymc3 as pm
from pymc3.distributions.timeseries import GaussianRandomWalk
import pandas as pd
import matplotlib.pyplot as plt

start_date = '2016-07-28'


def get_election_data( path ):
    # nightmare of date format without any years...
    # so we have to find where the months roll over
    rawdf = pd.read_csv( path + '2016_usa_presidential_rcp.csv' )[1:]
    dates = rawdf.Date.apply( lambda x: np.array(x.split('-')[-1].split('/')).astype(int) )
    diff_date = np.diff(dates.values)  # dtype: object
    diff_date = np.array( [x for x in diff_date] )  # dtype: int
    ind = np.argmax( diff_date[:, 0] )
    # 2016 before ind, 2015 after
    dates = [str(dates.iloc[i][0]) + '-' + str(dates.iloc[i][1]) + '-' + '2016' if i <= ind\
                else str(dates.iloc[i][0]) + '-' + str(dates.iloc[i][1]) + '-' + '2015' for i in range(len(dates))]
    dates = pd.to_datetime( dates )
    rawdf.Date = dates
    
    # now we have to aggregate by day
    polls = rawdf[['Date', 'Clinton (D)', 'Trump (R)']]
    polls['total sum'] = polls['Clinton (D)'] + polls['Trump (R)']
    # we will just make it so they add to 100
    polls['clinton'] = polls['Clinton (D)'] / polls['total sum']
    polls['trump'] = polls['Trump (R)'] / polls['total sum']
    # aggregate over all polls on that day
    avg_polls = polls.groupby('Date').mean()
    # interpolate these results
    avg_polls = avg_polls.resample('D').ffill()
    # now we can just look at the time series of clinton since trump = 1 - clinton
    return avg_polls.clinton[avg_polls.index >= start_date]


def model(red, election):

    # normalize because otherwise help
    # put it so minimal effort put in is equal to zero
    red = (red - red.mean()) / red.std()
    red = red - red.min()

    with pm.Model() as fit_model:
        
        # set up the model

        # hypers
        blue_mean = pm.Normal('blue mean', 0, 1)
        red_mean = pm.Normal('red mean', 0, 1)
        latent_sd = pm.Lognormal('sd', 0, 1)
        election_sd = pm.Lognormal('election sd', 0, 1)
        red_signal_sd = pm.Lognormal('tweet sd', 0, 1)
        
        # latents
        blue_latent = pm.GaussianRandomWalk('blue control',
                mu=blue_mean,
                sd=latent_sd,
                shape=len(red)-1
                )
        red_latent = pm.GaussianRandomWalk('red control',
                mu=red_mean,
                sd=latent_sd,
                shape=len(red)-1
                )
        
        # observed red as tweet counts
        red_signal = pm.Normal('red tweets', mu=red_latent, sd=red_signal_sd, observed=red[1:])

        # latent and observed election process
        election_latent = pm.GaussianRandomWalk('latent election', mu=blue_latent - red_latent,
                shape=len(red))
        election_signal = pm.LogitNormal('election', mu=election_latent, sd=election_sd, observed=election)

        # now draw from posterior
        
    with fit_model:
        trace = pm.sample(2000,
                chains=2,
                cores=8,
                progressbar=True)

        try:
            ppc = pm.sample_posterior_predictive(trace, samples=1000)
        except Exception as e:
            print(f'Weird shit with ppc, {e}')
            ppc = None
        return trace, fit_model, ppc


def main():

    datapath = '../data/red-blue-game/'
    electionpath = '../data/election_data/'
    tweetpath = '../data/russian_troll_tweets/russian-troll-tweets/'

    # get noisy signals
    red_data = pd.read_csv( tweetpath + 'russian-troll-tweets-restricted-count.csv' )
    red_data = red_data[red_data['Unnamed: 0'] >= start_date]

    # get election data
    election = get_election_data( electionpath )

    # now do some modeling
    posterior, fit_model, posterior_predictive = model(red_data.values[:, 1],
            election.values)

    with open('../data/red-blue-game/bayesian/posterior-nuts.pkl', 'wb') as f:
        pickle.dump(posterior, f)

    with open('../data/red-blue-game/bayesian/model-nuts.pkl', 'wb') as f:
        pickle.dump(fit_model, f)

    with open('../data/red-blue-game/bayesian/posterior-predictive-nuts.pkl', 'wb') as f:
        pickle.dump(posterior_predictive, f)
    

if __name__ == "__main__":
    main()
