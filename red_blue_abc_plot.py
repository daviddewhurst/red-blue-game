#!/usr/bin/env python

import sys

import joblib
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.special import eval_legendre

from red_blue_pdes import integrate as hjb_int


def logistic(x):
    return 1 / (1 + np.exp(-x))


def logit(x):
    return np.log( x / (1.0 - x) )


def main():

    run_num = int( sys.argv[1] )

    np.random.seed( 123 )  # for reproducibility, change this to whatever for marginally different distributions
    datapath = '../data/red-blue-game/' 
    figpath = '../paper/figures/red-blue-election-game/'

    # load the empirical data
    # enforce order red - blue - election for concatenation to higher-dim space
    latent_red_control = np.load(datapath + 'red_control_mean.npy')
    latent_blue_control = np.load(datapath + 'blue_control_mean.npy')
    real_election = np.load(datapath + 'logit_election_mean.npy')[:-1]  # one longer... and on (0, 1)

    # result of the optimization
    res = joblib.load( f'../data/red-blue-game/red-blue-abc/pde-MAP-gp-means-plus-std-newtime-{run_num}.gz' )

    lambduh_r = res['x'][0]
    lambduh_b = res['x'][1]
    sigma = res['x'][2]
    lcoef = int( len(res['x'][3:]) / 2 )
    red_coef = np.array( res['x'][3:3 + lcoef] )
    blue_coef = np.array( res['x'][3 + lcoef:] )
    x0 = logit(real_election[0])
    xmax = 2
    
    # recalculate Nt
    # it was roughly 8000 timesteps equals 364 days, but now we will reset this as we are only looking at length 102 vecs
    Nt = int( 8000 * len(latent_red_control) / 364 )
    Nt_downsamp = len(latent_red_control)


    vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=500,
                    finalr=red_coef,
                    finalb=blue_coef,
                    x0=x0)

    # set up values for interpolation
    orig_x = np.linspace(0, 1, Nt)  # dimensionality of the model's ts
    downsamp_x = np.linspace(0, 1, Nt_downsamp)
    
    # now we have to downsample this to length 364, since that's what our empirical data is
    # we can shift the vectors around but not scale them
    ur = -1.0 * np.array( [np.interp(downsamp_x, orig_x, r) for r in ur] ) 
    ub = np.array( [np.interp(downsamp_x, orig_x, b) for b in ub] )
    election = np.array( [np.interp(downsamp_x, orig_x, e) for e in logit_path] )  # on (-\infty, \infty)

    mean_ur = np.mean( ur, axis=0 )
    mean_ub = np.mean( ub, axis=0 )
    mean_election = np.mean( election, axis=0 )

    # make the joint plot

    fig, axes = plt.subplots(1, 3, figsize=(12, 4))
    ax, ax2, ax3 = axes

    for rr, bb in zip(ur[::10], ub[::10]):
        ax2.plot( rr, 'red', linewidth=0.5, alpha=0.5)
        ax2.plot( bb, 'blue', linewidth=0.5, alpha=0.5)
    ax2.plot(rr, 'red', linewidth=0.5, alpha=0.5, label='$\hat{u}_R(t)$')
    ax2.plot( bb, 'blue', linewidth=0.5, alpha=0.5, label='$\hat{u}_B(t)$')

    ax2.plot( latent_red_control, 'black', linewidth=3)
    ax2.plot(latent_blue_control, 'black', linewidth=3)
    ax2.plot( latent_red_control, 'red', label='$E[u_R(t)]$', linewidth=2)
    ax2.plot( latent_blue_control, 'blue', label='$E[u_B(t)]$', linewidth=2)
    
    ax2.legend(ncol=2, framealpha=0.)
    ax2.set_xlabel('$t$ (days)', fontsize=14)
    ax2.set_ylabel('$u(t)$ (control policy)', fontsize=14)
    ax2.tick_params(labelsize=14)
    ax2.set_xlim(0., len(latent_red_control))

    for ee in election[::10]:
        ax.plot( ee, 'darkgray', linewidth=0.5)
    ax.plot( ee, 'darkgray', linewidth=0.5, label='$\hat{x}(t)$')
    ax.plot( logit(real_election), 'black', label='$\mathrm{logit}\ Z(t)$')
    ax.legend(ncol=2, framealpha=0.)

    ax.set_xlabel('$t$ (days)', fontsize=14)
    ax.set_ylabel('$X_t$ (latent-space election)', fontsize=14)
    ax.tick_params(labelsize=14)
    ax.set_xlim(0., len(latent_red_control))
    
    # what do the final conditions look like?
    xvals = np.linspace(-xmax, xmax, vb.shape[-1])
    X = np.array([eval_legendre(n, xvals/xmax) for n in range(blue_coef.shape[0])]).T
    blue_fn = np.dot(X, blue_coef)
    red_fn = np.dot(X, red_coef)
    
    ax3.plot(xvals, blue_fn, 'blue')
    ax3.plot(xvals, red_fn, 'red')
    ax3.set_xlabel('$x$', fontsize=14)
    ax3.set_ylabel('$\Phi(x)$', fontsize=14)
    ax3.tick_params(labelsize=14)
    ax3.set_xlim(-xmax, xmax)
    
    plt.tight_layout()
    plt.savefig(f'./tmp/abc-plot-{run_num}.png')
    plt.close()


if __name__ == "__main__":
    main()
