#!/usr/bin/env python

from functools import partial 

import numpy as np
np.random.seed( 123 )
from scipy import integrate
import matplotlib.pyplot as plt


def wiener_paths(Nt, dt, v, sigma, x, time, reruns=100):
    paths = np.zeros( (reruns, Nt) )
    paths[:, 0] = x
    
    for n, t in enumerate(time[1:]):
        paths[:, n + 1] = paths[:, n] + dt * v(t)\
            + np.sqrt(dt) * sigma * np.random.randn(reruns)
        
    return paths


def pimc_hjb(
    xmin,
    xmax,
    Nx,
    t0,
    T,
    Nt,
    v,
    final,
    lambduh,
    sigma,
    reruns=1000
):
    """Compute MC approximation to value function for arbitrary 
    committed strategy by other player.
    
    :param v: callable, other player's strategy
    """
    space = np.linspace(xmin, xmax, Nx)
    time = np.linspace(t0, T, Nt)
    dt = time[1] - time[0]
    
    V = np.zeros( (Nt, Nx) )
    wm0 = None
    
    vsq = lambda t, v: v(t)**2
    vsq = partial(vsq, v=v)  # need to square the function for later
    
    for i, t in enumerate( time ):
        for j, x in enumerate( space ):
            wm = wiener_paths(
                len(time[i:]),
                dt,
                v,
                sigma,
                x,
                time[i:],
                reruns=reruns
            )
            # in our case, we just need the final time b/c no path dependent cost
            final_value = final(wm[:, -1])
            final_expect = np.mean( np.exp( -0.5 * sigma * final_value ) )
            final_integral = integrate.quad( vsq, time[i], time[-1] )[0]
            final_const = np.exp( -0.5 * lambduh / sigma * final_integral )
            V[i, j] = final_const * final_expect
            
            if (i == 0) and (j == int(space.shape[0]) / 2):
                wm0 = wm
            
    # now we have to actually get the value function out
    V = -2 * sigma**2 * np.log( V )
            
    return V, wm0


def main():

    xmin = -2
    xmax = -xmin
    Nx = 500
    t0 = 0
    T = 1
    Nt = 50
    v = lambda t, T, alpha: t**2
    v = partial(v, T=T, alpha=1)
    final = lambda x: np.where(np.absolute(x) < 1, -1, 1)
    lambduh = 2.
    sigma = 1
    reruns = 10000


    val_func_approx, paths = pimc_hjb(
        xmin,
        xmax,
        Nx,
        t0,
        T,
        Nt,
        v,
        final,
        lambduh,
        sigma,
        reruns=reruns
    )

    v0_approx = val_func_approx[0, :]
    vmid_approx = val_func_approx[int(val_func_approx.shape[0] * 0.75), :]
    vm1_approx = val_func_approx[-2, :]

    u0_approx = -0.5 * np.diff(v0_approx)
    umid_approx = -0.5 * np.diff(vmid_approx)
    um1_approx = -0.5 * np.diff(vm1_approx)

    x = np.linspace(xmin, xmax, Nx)
    time = np.linspace(t0, T, Nt)

    fig = plt.figure(figsize=(10, 10))
    
    axes = [plt.subplot2grid((2, 2), (0, i)) for i in range(2)]
    ax, ax2 = axes
    ax3 = plt.subplot2grid((2, 2), (1, 0), colspan=2)

    cmap=plt.get_cmap('copper', 3)

    ax.plot(x, v0_approx, color=cmap(0), label='$t=0$')
    ax.plot(x, vmid_approx, color=cmap(1), label='$t=0.75$')
    ax.plot(x, vm1_approx, color=cmap(2), label='$t=1-dt$')

    ax2.plot(x[1:], u0_approx, color=cmap(0), linewidth=0.5, alpha=0.5)
    ax2.plot(x[1:], umid_approx, color=cmap(1),
            linewidth=0.5, alpha=0.5)
    ax2.plot(x[1:], um1_approx, color=cmap(2), 
            linewidth=0.5, alpha=0.5)
    # need to smooth these
    nn = 15
    ma_k = np.ones(nn) / nn  # moving average kernel
    ax2.plot(x[1:], np.convolve(u0_approx, ma_k, mode='same'),
            color=cmap(0), linestyle='--', label='$t=0$')
    ax2.plot(x[1:], np.convolve(umid_approx, ma_k, mode='same'),
            color=cmap(1), linestyle='--', label='$t=0.75$')
    ax2.plot(x[1:], np.convolve(um1_approx, ma_k, mode='same'),
            color=cmap(2), linestyle='--', label='$t=1-dt$')

    ax.legend(fontsize=15)
    ax2.legend(fontsize=15)

    for path in paths[::50]:
        ax3.plot(time, path, 'k-', alpha=0.05)

    ax3.plot(time, v(time), 'w-', linewidth=3)
    ax3.plot(time, v(time), 'k-')
    ax3.set_xlim(min(time), max(time))

    for a in [ax, ax2]:
        a.set_xlabel('$x$ (latent space)', fontsize=20)
        a.tick_params(labelsize=15)
        
    ax2.yaxis.tick_right()
    ax2.yaxis.set_label_position('right')
    ax.set_ylabel('$V(x,t)$', fontsize=20)
    ax2.set_ylabel('$-\\frac{1}{2}\\frac{\partial V}{\partial x}(x,t)$',
                   fontsize=20)

    ax3.set_xlabel('$t$ (years)', fontsize=20)
    ax3.set_ylabel('$Y_t$', fontsize=20)
    ax3.tick_params(labelsize=15)

    plt.tight_layout()

    plt.savefig('../paper/figures/red-blue-election-game/path-integral-control-example.png',
            bbox_inches='tight', transparent=True)
    plt.savefig('../paper/figures/red-blue-election-game/path-integral-control-example.pdf', 
            bbox_inches='tight')
    plt.close()


if __name__ == "__main__":
    main()
