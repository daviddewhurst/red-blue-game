#!/usr/bin/env python

import json
import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def logistic(x, alpha=1):
    return 1 / (1 + np.exp(-alpha * x) )


def linlog(x):
    const = 0.3953  # about where x/10 = log_10 (x + 0.7)
    return np.where(np.absolute(x) < const, x/10, np.sign(x) * np.log10(np.absolute(x) + 0.7 ))


def main():
    datapath = '../data/red-blue-game/' 
    figpath = '../paper/figures/red-blue-election-game/'
    param_fnames = [x for x in os.listdir( datapath ) if x.endswith('.json') ]
    
    # load parameters and make into usable data structure
    params = []

    for fname in param_fnames:
        with open( datapath + fname, 'r' ) as f:
            p = json.load(f)
            p['run_num'] = fname.split('.')[0]
            params.append( p )

    param_df = pd.DataFrame( params )
    param_df.index = param_df.run_num
    del param_df['run_num']
    
    # set up plot
    # from this cursory sweep over possible parameters there are 3x3x3x3 = 81 data files
    # so we will set up a 9x9 grid

    fig, axes = plt.subplots(9, 9, figsize=(40, 40))

    # for later subplots
    sub_ind = int( 0.9 * int(param_df.iloc[0]['Nt']) )

    i = 0
    
    for row in axes:
        for ax in row:
            # load the particular file
            npz = np.load( datapath + str(i) + '.npz' )
            param_vec = param_df.loc[str(i)]
            mean_ur = -np.mean(npz['ur'], axis=0)[:-1]
            mean_ub = np.mean(npz['ub'], axis=0)[:-1]

            np.save(datapath + f'mean_red{i}', mean_ur)
            np.save(datapath + f'mean_blue{i}', mean_ub)
            
            ax.plot( 
                    npz['time'][:-1],
                    mean_ur,  # flip around so facing the same way
                    color='red',
                    linewidth=3
                    )
            ax.plot( 
                    npz['time'][:-1],
                    mean_ub,
                    color='blue',
                    linewidth=3
                    )

            title_str = f"$\\lambda_r = $ {round(param_vec.loc['lambduh_r'], 0)}"\
                    f" $\\lambda_b = {round(param_vec.loc['lambduh_b'], 0)}$\n"\
                    f" $\Phi_r = $ {param_vec.loc['finalr']}"\
                    f" $\Phi_b = $ {param_vec.loc['finalb']}"
            ax.set_title( title_str )
            
            samples = np.random.choice( npz['ub'].shape[0], size=2, replace=False )

            for s in samples:
                ax.plot( 
                        npz['time'][:-1],
                        -npz['ur'][s, :-1],  # flip around so facing the same way
                        color='red',
                        linewidth=0.25,
                        alpha=0.25
                        )
                ax.plot( 
                        npz['time'][:-1],
                        npz['ub'][s, :-1],
                        color='blue',
                        linewidth=0.25,
                        alpha=0.25
                        )

            # save the examples for later plotting
            np.save(datapath + f'red-example{i}{s}', -npz['ur'][s, :-1])
            np.save(datapath + f'blue-example{i}{s}', npz['ub'][s, :-1])

            # inset plot if need be
            lower, upper = ax.get_ylim()

            if upper - lower > 1.5:
                # we need inset plot
                # now see which quadrant it should be in 
                
                left, right = ax.get_xlim()
                if np.minimum( mean_ur, mean_ub )[0] - lower > upper - np.maximum( mean_ur, mean_ub )[0]:
                    # should be below
                    if np.maximum(mean_ur, mean_ub)[0] > np.maximum(mean_ur, mean_ub)[-1]:
                        inset_ax = inset_axes( ax, width='40%', height='40%', loc=4, borderpad=2 ) 
                    else:
                        inset_ax = inset_axes( ax, width='40%', height='40%', loc=3, borderpad=2 ) 
                else:
                    if np.maximum(mean_ur, mean_ub)[0] > np.maximum(mean_ur, mean_ub)[-1]:
                        inset_ax = inset_axes( ax, width='40%', height='40%', loc=1, borderpad=2 )
                    else:
                        inset_ax = inset_axes( ax, width='40%', height='40%', loc=2, borderpad=2 )

                inset_ax.plot(
                        np.log10(npz['time'][sub_ind:-1]),
                        linlog( mean_ur[sub_ind:] ), 
                        color='red',
                        linewidth=1
                    )
                inset_ax.plot( 
                        np.log10(npz['time'][sub_ind:-1]),
                        linlog( mean_ub[sub_ind:] ),
                        color='blue',
                        linewidth=1
                    )

                for s in samples:
                    inset_ax.plot( 
                        np.log10(npz['time'][sub_ind:-1]),
                        linlog( -npz['ur'][s, sub_ind:-1] ),  # flip around so facing the same way
                        color='red',
                        linewidth=0.25,
                        alpha=0.25
                        )
                    inset_ax.plot( 
                        np.log10(npz['time'][sub_ind:-1]),
                        linlog( npz['ub'][s, sub_ind:-1] ),
                        color='blue',
                        linewidth=0.25,
                        alpha=0.25
                        )

                inset_ax.set_xlabel('$\log_{10} t$')
                inset_ax.set_ylabel('abslog$_{10}$(u)')
                inset_ax.set_yticks([])
                inset_ax.set_yticklabels([])
                     
            i += 1

    for a in axes[:, 0]:
        a.set_xticks([])
        a.set_xticklabels([])
        a.set_ylabel('$u$', fontsize=15)

    for a in axes[-1, :]:
        a.set_xlabel('$t$ (years)', fontsize=15)

    for row in axes[:-1, 1:]:
        for a in row:
            a.set_xticks([])
            a.set_xticklabels([])

    for row in axes:
        for a in row:
            a.tick_params(labelsize=15)

    plt.savefig(figpath + 'mean_strategy_plot.png',
            bbox_inches='tight',
            transparent=True)
    plt.close()



if __name__ == "__main__":
    main()
