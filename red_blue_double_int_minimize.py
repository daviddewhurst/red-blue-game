#!/usr/bin/env python

import numpy as np
from scipy import optimize, integrate, special
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


def main():
    v = 0.01
    T = 1.
    s = 0.75
    Nt = 100
    div = 1
    ts = np.linspace(0.5, 0.9975, Nt)
    n_to_plot = Nt // div

    a_vals = np.zeros_like( ts )
    uas = []
    us = []

    for i, t in enumerate( ts ):
        def f( a ):
            def f_to_integrate(x, a, v, T, t, s):
                move = x + v * (T - t)
                ua = -a / (2. * np.cosh( a * move )**2.)  # -a/2 * sech^2( ... )
                u_pre = -1. * np.sqrt( 2 * s**2. / (np.pi * (T - t)) )
                u_num = np.exp( -move**2. / (2 * s**2. *(T - t)) )
                u_den = 1./ np.tanh(1./(2 * s**2.)) + special.erf( -move / np.sqrt(2 * s**2.*(T - t)) )  # coth( ... )
                u = u_pre * u_num / u_den

                duda = 0.5 / np.cosh( a * move )**2. * ( -1 + 2 * a * move * np.tanh(a * move) )

                return (ua - u) * duda

            f_to_integrate_args = (a, v, T, t, s)

            return integrate.quad(
                f_to_integrate,
                -np.inf,
                np.inf,
                args=f_to_integrate_args
                    )[0]

        a = optimize.root(f, x0=1.).x[0]
        a_vals[i] = a
        
        # let's make a plot
        if (i + 1) % div == 0:
            x = np.linspace(-2, 2, 100)
            move = x + v * (T - t)
            ua = -a / (2. * np.cosh( a * move )**2.)  # -a/2 * sech^2( ... )
            u_pre = -1. * np.sqrt( 2 * s**2. / (np.pi * (T - t)) )
            u_num = np.exp( -move**2. / (2 * s**2. *(T - t)) )
            u_den = 1./ np.tanh(1./(2 * s**2.)) + special.erf( -move / np.sqrt(2 * s**2.*(T - t)) )  # coth( ... )
            u = u_pre * u_num / u_den
            uas.append( ua )
            us.append( u )


    fig, ax = plt.subplots(figsize=(10, 4))
    colors = plt.get_cmap('magma', n_to_plot + 1)
    
    for i, (u, ua) in enumerate( zip(us, uas) ):
        if i % 10 == 0:
            ax.plot(x, ua, color=colors(i))
            ax.plot(x, u, color=colors(i), linestyle='--')

    ax.set_xlabel('$x$', fontsize=15)
    ax.set_ylabel('$u(x,t)$', fontsize=15)
    ax.tick_params(labelsize=15)

    # colorbar for time
    sm = plt.cm.ScalarMappable(cmap=plt.cm.magma, norm=plt.Normalize(vmin=min(ts), vmax=1))
    sm._A = []  # do NOT ask, matplotlib hell
    cbar = plt.colorbar( sm )
    cbar.ax.set_ylabel('$t$ (years)', fontsize=15)
    cbar.ax.tick_params(labelsize=15)


    axins = inset_axes(ax, width="30%", height="40%", loc=4)
    for i, (u, ua) in enumerate( zip(us, uas) ):
        if (i + 1) % 10 == 0:
            axins.plot(x, ua, color=colors(i - 1))
            axins.plot(x, u, color=colors(i - 1), linestyle='--')
    #axins.yaxis.tick_right()
    #axins.yaxis.set_label_position("right")
    axins.xaxis.tick_top()
    axins.xaxis.set_label_position("top")
    axins.set_xlim(min(x), max(x))
    axins.set_xlabel('$x$', fontsize=12)
    axins.set_ylabel('$u(x,t)$', fontsize=12)
    axins.tick_params(labelsize=12)
    axins.set_yscale('symlog', linthresh=1)

    axins2 = inset_axes(ax, width="30%", height="40%", loc=3)
    axins2.plot(ts, a_vals, 'k-')
    axins2.set_xlabel('$t$ (years)', fontsize=12)
    axins2.set_ylabel('$a(t)$', fontsize=12)
    axins2.tick_params(labelsize=12)
    axins2.yaxis.tick_right()
    axins2.yaxis.set_label_position("right")
    axins2.xaxis.tick_top()
    axins2.xaxis.set_label_position("top")
    axins2.set_xlim(min(ts), 1.05)
    axins2.set_yscale('log')
    axins2.set_yticks([2, 10, 20])
    axins2.set_yticklabels(['$2\\times 10^0$', '$10^1$', '$2\\times 10^1$'])


    plt.tight_layout()
    plt.savefig('./tmp/a_val_opt.png',
            bbox_inches='tight',
            transparent=True)
    plt.savefig('./tmp/a_val_opt.pdf',
            bbox_inches='tight')



if __name__ == "__main__":
    main()
