#!/usr/bin/env python

from functools import partial
from itertools import product

import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.ticker import LogFormatterMathtext


def phi_general( x, a ):
    return np.tanh( a * x )


def main():
    v = 0.01
    T = 1.
    s = 0.75
    a_vals = np.logspace(-3, 5, 60)
    x_vals = np.linspace(-1.5, 1.5, 60)

    ntval = 8
    t_vals = np.array( [1. - 0.5 ** n for n in range(1, ntval + 1)] )

    fig, axes = plt.subplots(2, int(ntval / 2), figsize=(12, 4))
    fig.subplots_adjust(left=0.02, bottom=0.06, right=0.95, top=0.94, wspace=0.05)
    axes = axes.flatten()

    for ax, t in zip(axes, t_vals):
        result_array = np.zeros( (a_vals.shape[0], x_vals.shape[0]) )
        for i, a in enumerate( a_vals ):
            for j, x in enumerate( x_vals ):
                def f(y, x, v, T, t, s, a):
                    return np.exp( -0.5 / s**2. *\
                        ( phi_general(y, a) + ((y - x) - v * (T - t))**2. / (T - t)))

                res = integrate.quad( 
                    f,
                    -np.inf,
                    np.inf,
                    args=(x, v, T, t, s, a)
                    )
                result_array[i, j] = res[0]

        A, X = np.meshgrid(a_vals, x_vals)
        im = ax.pcolormesh(A, X, result_array.T, cmap='magma_r', norm=LogNorm())
        ax.set_xlabel('$a$')
        ax.set_ylabel('$x$')
        ax.set_xscale('log')
        cbar = fig.colorbar(im,
                orientation='vertical',
                ax=ax)
        cbar.ax.set_ylabel(f'$\\varphi(x, t)$')
        ax.set_title(f'$t = {round(t, 4)}$')

    plt.tight_layout()
    plt.savefig('../paper/figures/red-blue-election-game/red-blue-a-x-t-constant.png',
            bbox_inches='tight',
            transparent=True)
    plt.savefig('../paper/figures/red-blue-election-game/red-blue-a-x-t-constant.pdf',
            bbox_inches='tight')
    plt.close()






if __name__ == "__main__":
    main()
