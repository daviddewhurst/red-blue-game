#!/usr/bin/env python

import os


paperpath = '../paper/red_blue_arxiv/'
figpath = 'figures/red-blue-election-game/bifurcation/'

redconds = {
        'tanh' : '$\\tanh(x)$',
        'linear' : '$x$',
        'piecewise' : '$2[\Theta(x) - \Theta(-x)]$'
        }
blueconds = {
        'quad' : '$\\frac{1}{2}x^2$',
        'piecewise' : '$2[\Theta(|x| - 0.1) - \Theta(0.1 - |x|)]$',
        'asym' : '$\\frac{1}{2}x^2\Theta(-x)$'
        }


def make_appendix( kind ):
    if kind == 'std':
        fnames = sorted( [x for x in os.listdir(paperpath + figpath)\
            if ('mean' not in x) and ('abc' not in x) and ('montage' not in x) and x.endswith('png')] )
    elif kind == 'mean':
        fnames = sorted( [x for x in os.listdir(paperpath + figpath)\
            if ('mean' in x) and ('montage' not in x) and x.endswith('png')] )


    begin_figure_text = '''\\begin{figure}[!ht]
    \centering'''
    end_figure_text = '\end{figure}'

    with open(paperpath + f'red_blue_lambda_{kind}_appendix.tex', 'w') as f:
        for i, fname in enumerate( fnames ):
            functions = fname.split('.')[0].split('-')[-2:]
            print(
                begin_figure_text, 
                file=f
                    )
            print(
                '\\includegraphics[width=0.9\\linewidth]{{{}}}'.format(figpath + fname),
                file=f
                    )
            print(
                f'\\caption{{Parameter sweep over coupling parameters'
                f' $\\lambda_{{\\mathrm{{R}}}}, \\lambda_{{\\mathrm{{B}}}}$'
                f' with Red final condition $\\Phi_{{\mathrm{{R}}}}(x) =$ {redconds[functions[0]]}'
                f' and Blue final condition $\\Phi_{{\mathrm{{B}}}}(x) =$ {blueconds[functions[1]]}.'
                f' Intensity of color corresponds to {kind} of control policy.}}',
                file=f
                    )
            print(
                end_figure_text,
                file=f
                    )
            if i % 2 == 1:
                print(
                    '\\clearpage',
                    file=f
                        )


if __name__ == "__main__":
    
    make_appendix( 'mean' )
    make_appendix( 'std' )
