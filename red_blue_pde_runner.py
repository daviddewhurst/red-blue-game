#!/usr/bin/env python

import multiprocessing
import subprocess
import itertools

import numpy as np


def job( args ):

    lr, lb, bthresh, finalr, finalb, runnum = args
    
    subprocess.check_call( 
            [
                './red_blue_pdes.py',
                '--id',
                str(runnum),
                '--reruns',
                '1000',
                '--nt',
                '8000',
                '--dx',
                '0.025',
                '--xbound',
                '3',
                '--lr',
                str(lr),
                '--lb',
                str(lb),
                '--sigma',
                '0.6',
                '--bthresh',
                str(bthresh),
                '--finalcondr',
                finalr,
                '--finalcondb',
                finalb
            ]
        )


if __name__ == '__main__':

    lrs = np.linspace(0, 2, 3)
    lbs = np.linspace(0, 2, 3)
    bts = [0.1] 
    finalrs = ['linear', 'piecewise', 'tanh']
    finalbs = ['quad', 'piecewise', 'asym']

    params = itertools.product( lrs, lbs, bts, finalrs, finalbs )
    params = [ list(x) + [i] for i, x in enumerate(list(params)) ]

    print(len(params))

    pool = multiprocessing.Pool( 8 )
    pool.map( job, params )
