#!/usr/bin/env python

import pickle

import numpy as np
import matplotlib.pyplot as plt
import pymc3 as pm
import pandas as pd

from red_blue_bayesian_model import get_election_data

figpath = '../paper/figures/red-blue-election-game/'
start_date = '2016-07-28'


def logit(x):
    return np.log( x / (1.0 - x) )


def plot_election( election_trace, latent_elec_mean, elec_mean ):
    for x in election_trace['latent election'][::20]:
        plt.plot(x, 'lightgray')
    plt.plot(x, 'lightgray', label='$x_t$')
    
    plt.plot(latent_elec_mean, 'darkgray', label='$E[x_t]$')
    plt.plot(logit( elec_mean ), 'k--', label='$\mathrm{logit}\ Z_t$')

    plt.legend(fontsize=13)
    plt.xlabel('$t$ (days)', fontsize=13)
    plt.ylabel('$x_t$ (latent electoral process)', fontsize=13)
    plt.gca().tick_params(labelsize=13)
    plt.xlim(0, len(elec_mean))

    plt.tight_layout()
    plt.savefig( figpath + 'latent-election-only.pdf', bbox_inches='tight')
    plt.savefig( figpath + 'latent-election-only.png', bbox_inches='tight',
            transparent=True)
    plt.close()


def plot_control_vectors( election_trace, blue_mean, red_mean ):

    for x in election_trace['blue control'][::20]:
        plt.plot(x, 'blue', linewidth=0.5, alpha=0.05)

    for x in election_trace['red control'][::20]:
        plt.plot(x, 'red', linewidth=0.5, alpha=0.05)

    plt.plot(blue_mean, 'blue', label='$E[u_B]$')
    plt.plot(red_mean, 'red', label='$E[u_R]$')

    plt.legend(fontsize=13)
    plt.xlabel('$t$ (days)', fontsize=13)
    plt.ylabel('$u$ (control policy)', fontsize=13)
    plt.gca().tick_params(labelsize=13)
    plt.xlim(0, len(blue_mean))

    plt.tight_layout()
    plt.savefig( figpath + 'latent-control-vectors.pdf', bbox_inches='tight')
    plt.savefig( figpath + 'latent-control-vectors.png', bbox_inches='tight',
            transparent=True)
    plt.close()


def plot_tweets( election_ppc, tweets ):

    for x in election_ppc['red tweets'][::20]:
        plt.plot(x, 'red', linewidth=0.5, alpha=0.05)
    
    plt.plot(tweets.values, 'red')
   
    plt.xlabel('$t$ (days)', fontsize=13)
    plt.ylabel('Renormalized tweet activity', fontsize=13)
    plt.gca().tick_params(labelsize=13)
    plt.xlim(0, len(tweets))

    plt.tight_layout()
    plt.savefig( figpath + 'renormalized-tweet-activity.pdf', bbox_inches='tight')
    plt.savefig( figpath + 'renormalized-tweet-activity.png', bbox_inches='tight',
            transparent=True)
    plt.close()


def main():
    # load election
    with open('../data/red-blue-game/bayesian/posterior-nuts.pkl', 'rb') as f:
        election_trace = pickle.load(f)
        
    with open('../data/red-blue-game/bayesian/model-nuts.pkl', 'rb') as f:
        election_model = pickle.load(f)
        
    with open('../data/red-blue-game/bayesian/posterior-predictive-nuts.pkl', 'rb') as f:
        election_ppc = pickle.load(f)
        
    actual_election = get_election_data( '../data/election_data/' )

    normdatared = pd.read_csv('../data/red-blue-game/normdatared.csv').iloc[:-1]
    normdatared.columns = ['Date', 'data red']
    normdatared = normdatared[normdatared.Date >= start_date]
    normdatared.index = normdatared.Date
    del normdatared['Date']
    red = (normdatared - normdatared.mean()) / np.std(normdatared)
    red = red - red.min()

    # save dot file for plotting later
    im = pm.model_to_graphviz(election_model)
    im.name = 'election_model'
    im.save(directory='../data/red-blue-game/bayesian/')

    # save control vectors for later analysis
    blue_mean = election_trace['blue control'].mean(axis=0)
    red_mean = election_trace['red control'].mean(axis=0)
    latent_elec_mean = election_trace['latent election'].mean(axis=0)
    elec_mean = election_ppc['election'].mean(axis=0)
    red_ppc = election_ppc['red tweets'].mean(axis=0)

    np.save('../data/red-blue-game/blue_control_mean', blue_mean)
    np.save('../data/red-blue-game/red_control_mean', red_mean)
    np.save('../data/red-blue-game/latent_election_mean', latent_elec_mean)
    np.save('../data/red-blue-game/logit_election_mean', elec_mean)

    grs = pm.diagnostics.gelman_rubin( election_trace )
    for k, v in grs.items():
        max_v = np.max( np.array(v) )  # some are just scalars instead of arrays
        print(f'Max GRS for {k}: {max_v}')

    # plot just the election itself in latent space
    plot_election( election_trace, latent_elec_mean, elec_mean )

    # plot the latent control vectors
    plot_control_vectors( election_trace, blue_mean, red_mean )

    # plot the renormalized tweet time series
    plot_tweets( election_ppc, red )



if __name__ == "__main__":
    main()
