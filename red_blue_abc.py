#!/usr/bin/env python

"""Optimizes parameter values for PDE red-blue system.
"""

import sys

import json
import os
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
from functools import partial

import numpy as np
np.seterr(over='raise', under='raise', divide='raise')  # so we catch when pde model blows up
import pandas as pd
import matplotlib.pyplot as plt
import joblib
from skopt import gp_minimize #, gbrt_minimize, dummy_minimize
from skopt import dump as skopt_dump

from red_blue_pdes import integrate as hjb_int

start_date = '2016-07-28'


def logistic(x):
    return 1 / (1 + np.exp(-x))


def logit(x):
    return np.log( x / (1.0 - x) )


def main():
    np.random.seed( 123 )  # for reproducibility, change to whatever you like
    datapath = '../data/red-blue-game/' 
    figpath = '../paper/figures/red-blue-election-game/'

    # load the empirical data
    latent_red_control = np.load(datapath + 'red_control_mean.npy')
    latent_blue_control = np.load(datapath + 'blue_control_mean.npy')
    real_election = np.load(datapath + 'logit_election_mean.npy')[:-1]  # one longer... and on (0, 1)

    #######################
    ### input arguments ###
    run_num = int( sys.argv[1] )
    std_multiplier = float( sys.argv[2] )
    ncoef = int( sys.argv[3] )
    #######################

    def f( x,
            latent_red_control,
            latent_blue_control,
            real_election,
            x0 ):
        """
        This is the approximation to the posterior density of parameter vector theta 
        """
        lambduh_r = x[0]
        lambduh_b = x[1]
        sigma = x[2]
        n_coef = int(len(x[3:]) / 2)
        y = x[3:]
        red_coef = np.array( y[:n_coef] )
        blue_coef = np.array( y[n_coef:] )
        # recalculate Nt
        # it was roughly 8000 timesteps equals 364 days, but now we will reset this as we are only looking at length 102 vecs
        Nt = int( 8000 * len(latent_red_control) / 364 )
        Nt_downsamp = len(latent_red_control)
        xmax = 2

        # integrate a whole model
        # some values of parameters will blow it up
        try:
            vb, vr, x, time, ur, ub, logit_path, uncontrolled_path \
                = hjb_int(Nt=Nt,
                    dx=0.025,
                    xmin=-xmax,
                    xmax=xmax,
                    lambduh_r=lambduh_r,
                    lambduh_b=lambduh_b,
                    sigma=sigma,
                    reruns=2000,
                    finalr=red_coef,
                    finalb=blue_coef,
                    x0=x0)

            # set up values for interpolation
            orig_x = np.linspace(0, 1, Nt)  # dimensionality of the model's ts
            downsamp_x = np.linspace(0, 1, Nt_downsamp)
            
            # now we have to downsample this to length Nt_downsamp, since that's what our empirical data is
            ur = -1.0 * np.array( [np.interp(downsamp_x, orig_x, r) for r in ur] )  # just represent as positive 
            ub = np.array( [np.interp(downsamp_x, orig_x, b) for b in ub] )
            election = np.array( [np.interp(downsamp_x, orig_x, e) for e in logit_path] )  # on (-\infty, \infty)

            mean_ur = np.mean( ur, axis=0 )
            mean_ub = np.mean( ub, axis=0 )
            mean_election = np.mean( election, axis=0 )

            # compute the three loss functions
            Lr = np.mean( (mean_ur[:-1] - latent_red_control[:-1])**2. )
            Lb = np.mean( (mean_ub[:-1] - latent_blue_control[:-1])**2. )
            Lx = np.mean( (mean_election - logit(real_election))**2. )

            Lrs = Lr + std_multiplier * np.std( ur )  # std is of *distribution*, not temporal std of mean path
            Lbs = Lb + std_multiplier * np.std( ub )
            Lxs = Lx + std_multiplier * np.std( election )

            print(f'Lr = {Lrs}')
            print(f'Lb = {Lbs}')
            print(f'Lx = {Lxs}')

            return Lrs + Lbs + Lxs
        
        except FloatingPointError as e:
            print('Floating point error in PDE evaluation')
            print(e)
            return 11e7  # some very large value

    f_for_gp = partial(f,
            latent_red_control=latent_red_control,
            latent_blue_control=latent_blue_control,
            real_election=real_election,
            x0=logit(real_election[0]))

    bounds = [ (0, 4.), (0, 4.), (0.5, 2)]
    bounds.extend( [(-4., 4.) for _ in range(2 * ncoef)] )  # put in weak bounds on legendre coefs
    bounds = np.array( bounds )

    nrs = 100
    n = 50
    nc = n + nrs

    res = gp_minimize(
            f_for_gp,
            bounds,
            verbose=True,
            n_random_starts=nrs,
            n_calls=nc,
            )
    to_save = {k : v for k, v in res.items() if k != 'specs'}  # can't pickle function in specs

    joblib.dump(
            to_save,
            f'../data/red-blue-game/red-blue-abc/pde-MAP-gp-means-plus-std-newtime-{run_num}.gz',
            ) 
    joblib.dump( 
            {
                'std_multiplier' : std_multiplier,
                'ncoef' : ncoef
            },
        f'../data/red-blue-game/red-blue-abc/pde-MAP-gp-means-plus-std-newtime-{run_num}-STATS.gz',
            )

if __name__ == "__main__":
    main()
